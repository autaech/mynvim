require('options')
require('colors')

return require('packer').startup(function()
    use 'wbthomason/packer.nvim'
    use 'kdheepak/monochrome.nvim'
    use 'mattn/emmet-vim'
    use 'tpope/vim-commentary'
    use 'alvan/vim-closetag'
    use {
	"windwp/nvim-autopairs",
    config = function() require("nvim-autopairs").setup {} end
  }
    use {
    'vimwiki/vimwiki',
    config = function()
        vim.g.vimwiki_list = {
            {
                path = '~/documents/vimwiki/',
                syntax = 'markdown',
                ext = '.md',
            }
        }
    end
  }
end)
